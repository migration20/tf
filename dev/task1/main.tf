//create ssh key
resource "aws_key_pair" "key-tf" {
  key_name   = "deployer-key"
  public_key = file("${path.module}/id_rsa.pub")
}

resource "aws_instance" "web" {

    ami = "ami-09d56f8956ab235b3"
    instance_type = "t3.micro"
    key_name = "${aws_key_pair.key-tf.key_name}"
    tags = {
      
        Name = "TF-Cloud instance"
    
    }
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"

    }
  }
}

provider "aws" {

  region  = "us-east-1"

}

